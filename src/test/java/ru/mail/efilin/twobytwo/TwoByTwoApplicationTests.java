package ru.mail.efilin.twobytwo;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
class TwoByTwoApplicationTests {
	private TestRestTemplate restTemplate;
	private URL base;
	@LocalServerPort
	private int port;

	@Test
	void contextLoads() {
	}

	@Before
	public void setUp() throws MalformedURLException {
		restTemplate = new TestRestTemplate("admin", "admin");
		base = new URL("http://localhost:" + port);
	}

	@Test
	public void whenLoggedUserRequestsHomePage_ThenSuccess()
			throws IllegalStateException {
		ResponseEntity<String> response =
				restTemplate.getForEntity(base.toString(), String.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(Objects.requireNonNull(response.getBody()).contains("You need to enable JavaScript to run this app."));
	}

	@Test
	public void whenUserWithWrongCredentials_thenUnauthorizedPage() {
		restTemplate = new TestRestTemplate("admin", "wrongpassword");
		ResponseEntity<String> response = restTemplate.getForEntity(base.toString(), String.class);
		assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
		assertTrue(Objects.requireNonNull(response.getBody()).contains("Unauthorized"));
	}
}
