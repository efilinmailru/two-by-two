package ru.mail.efilin.twobytwo.repositories;

import java.util.List;
import java.util.Optional;

public interface EntityRepository<E> {

    E save(E entity);

    List<E> findAll();

    Optional<E> findById(long id);

    void deleteById(long id);
}
