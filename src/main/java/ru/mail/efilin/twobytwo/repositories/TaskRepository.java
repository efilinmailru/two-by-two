package ru.mail.efilin.twobytwo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mail.efilin.twobytwo.entities.Task;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long>, EntityRepository<Task> {

    Optional<Task> findByCategoryIdAndTask(long categoryId, String task);

    List<Task> findAllByCategoryId(long categoryId);
}
