package ru.mail.efilin.twobytwo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mail.efilin.twobytwo.entities.Category;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long>, EntityRepository<Category> {

    Optional<Category> findByName(String name);
}
