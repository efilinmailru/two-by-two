package ru.mail.efilin.twobytwo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mail.efilin.twobytwo.entities.User;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, EntityRepository<User> {

    Optional<User> findByEmail(String email);

    Optional<User> findByName(String name);
}
