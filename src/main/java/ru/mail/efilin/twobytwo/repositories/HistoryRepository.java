package ru.mail.efilin.twobytwo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mail.efilin.twobytwo.entities.History;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface HistoryRepository extends JpaRepository<History, Long>, EntityRepository<History> {

    List<History> findAllByUserIdAndCategoryIdAndTaskIdAndResultIdAndCreated(
            long userId, long categoryId, long taskId, long resultId, LocalDate created);
}
