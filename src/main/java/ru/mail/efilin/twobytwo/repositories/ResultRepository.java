package ru.mail.efilin.twobytwo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mail.efilin.twobytwo.entities.Result;
import ru.mail.efilin.twobytwo.entities.Role;

import java.util.Optional;

@Repository
public interface ResultRepository extends JpaRepository<Result, Long>, EntityRepository<Result> {

    Optional<Result> findByName(String name);
}
