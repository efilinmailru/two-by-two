package ru.mail.efilin.twobytwo.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.mail.efilin.twobytwo.dtos.back.responses.Dtos;
import ru.mail.efilin.twobytwo.dtos.back.responses.RoleDto;
import ru.mail.efilin.twobytwo.dtos.back.responses.UserDto;
import ru.mail.efilin.twobytwo.dtos.front.reqiests.UserRequest;
import ru.mail.efilin.twobytwo.entities.Role;
import ru.mail.efilin.twobytwo.entities.User;
import ru.mail.efilin.twobytwo.services.RoleService;
import ru.mail.efilin.twobytwo.services.UserService;

import java.util.List;

@Api(tags = "Пользователи")
@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;
    private final RoleService roleService;

    @Autowired
    public UserController(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    //    @PreAuthorize("hasPermission(@clientController.entityName, 'write' )")
    @ApiOperation(value = "Создать пользователя")
    @PostMapping("/create")
    public ResponseEntity<UserDto> create(@ModelAttribute UserRequest userRequest) {
        User user = userService.create(userRequest);
        return new ResponseEntity<>(userService.transferToDto(user), HttpStatus.RESET_CONTENT);
    }

    @ApiOperation(value = "Получить пользователя по id")
    @GetMapping("")
    public ResponseEntity<UserDto> getById(@RequestParam(name = "id") String id) {
        User user = userService.find(Long.parseLong(id));
        return new ResponseEntity<>(userService.transferToDto(user), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Получить пользователя по email")
    @GetMapping("/email")
    public ResponseEntity<UserDto> findByEmail(@RequestParam(name = "email") String email) {
        User user = userService.findByEmail(email);
        return new ResponseEntity<>(userService.transferToDto(user), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Получить все роли пользователя")
    @GetMapping("/roles")
    public ResponseEntity<Dtos<RoleDto>> findAllRoles(@RequestParam(name = "userid") String userId) {
        List<Role> roles = userService.findAllRolesByUserId(Long.parseLong(userId));
        return new ResponseEntity<>(roleService.transferToDtos(roles), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Получить всех пользователей")
    @GetMapping("/all")
    public ResponseEntity<Dtos<UserDto>> findAll() {
        List<User> users = userService.findAll();
        return new ResponseEntity<>(userService.transferToDtos(users), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Сохранить пользователя")
    @PostMapping("/update")
    public ResponseEntity<UserDto> update(@ModelAttribute UserRequest userRequest) {
        User user = userService.update(userRequest);
        return new ResponseEntity<>(userService.transferToDto(user), HttpStatus.RESET_CONTENT);
    }

    @ApiOperation(value = "Удалить пользователя")
    @GetMapping("/delete")
    public ResponseEntity<UserDto> delete(@RequestParam(name = "id") String id) {
        User user = userService.delete(Long.parseLong(id));
        return new ResponseEntity<>(userService.transferToDto(user), HttpStatus.MOVED_PERMANENTLY);
    }
}
