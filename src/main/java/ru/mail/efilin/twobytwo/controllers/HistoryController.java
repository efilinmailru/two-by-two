package ru.mail.efilin.twobytwo.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.mail.efilin.twobytwo.dtos.back.responses.Dtos;
import ru.mail.efilin.twobytwo.dtos.back.responses.HistoryDto;
import ru.mail.efilin.twobytwo.dtos.front.reqiests.HistoryRequest;
import ru.mail.efilin.twobytwo.entities.History;
import ru.mail.efilin.twobytwo.services.HistoryService;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/history")
@Api(tags = "История")
public class HistoryController {
    private final HistoryService historyService;

    public HistoryController(HistoryService historyService) {
        this.historyService = historyService;
    }

    //    @PreAuthorize("hasPermission(@clientController.entityName, 'write' )")
    @ApiOperation(value = "Создать историю")
    @PostMapping("/create")
    public ResponseEntity<HistoryDto> create(@ModelAttribute HistoryRequest historyRequest) {
        History history = historyService.create(historyRequest);
        return new ResponseEntity<>(historyService.transferToDto(history), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Получить историю по id")
    @GetMapping("")
    public ResponseEntity<HistoryDto> getById(@RequestParam(name = "id") long id) {
        History history = historyService.find(id);
        return new ResponseEntity<>(historyService.transferToDto(history), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Получить историю по параметрам")
    @GetMapping("/all")
    public ResponseEntity<Dtos<HistoryDto>> findAll(@RequestParam(name = "userId") long userId,
                                                 @RequestParam(name = "categoryId") long categoryId,
                                                 @RequestParam(name = "taskId") long taskId,
                                                 @RequestParam(name = "resultId") long resultId,
                                                 @RequestParam(name = "created") LocalDate created) {
        List<History> histories = historyService.findAll(userId, categoryId, taskId, resultId, created);
        return new ResponseEntity<>(historyService.transferToDtos(histories), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Сохранить историю")
    @PostMapping("/update")
    public ResponseEntity<HistoryDto> update(@ModelAttribute HistoryRequest historyRequest) {
        History history = historyService.update(historyRequest);
        return new ResponseEntity<>(historyService.transferToDto(history), HttpStatus.RESET_CONTENT);
    }

    @ApiOperation(value = "Удалить историю")
    @GetMapping("/delete")
    public ResponseEntity<HistoryDto> delete(@RequestParam(name = "id") long id) {
        History history = historyService.delete(id);
        return new ResponseEntity<>(historyService.transferToDto(history), HttpStatus.MOVED_PERMANENTLY);
    }
}
