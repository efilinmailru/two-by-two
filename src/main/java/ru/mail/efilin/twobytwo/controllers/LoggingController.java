package ru.mail.efilin.twobytwo.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/log")
public class LoggingController {

    @GetMapping("/trace")
    public void trace(@RequestParam(name = "message") String message) {
        log.trace(message);
    }

    @GetMapping("/debug")
    public void debug(@RequestParam(name = "message") String message) {
        log.debug(message);
    }

    @GetMapping("/info")
    public void info(@RequestParam(name = "message") String message) {
        log.info(message);
    }

    @GetMapping("/warn")
    public void warn(@RequestParam(name = "message") String message) {
        log.warn(message);
    }

    @GetMapping("/error")
    public void error(@RequestParam(name = "message") String message) {
        log.error(message);
    }
}
