package ru.mail.efilin.twobytwo.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.mail.efilin.twobytwo.dtos.back.responses.Dtos;
import ru.mail.efilin.twobytwo.dtos.back.responses.TaskDto;
import ru.mail.efilin.twobytwo.dtos.front.reqiests.TaskRequest;
import ru.mail.efilin.twobytwo.entities.Task;
import ru.mail.efilin.twobytwo.services.TaskService;

import java.util.List;

@Api(tags = "Задания")
@RestController
@RequestMapping("/tasks")
public class TaskController {
    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    //    @PreAuthorize("hasPermission(@clientController.entityName, 'write' )")
    @ApiOperation(value = "Создать задание")
    @PostMapping("/create")
    public ResponseEntity<TaskDto> create(@ModelAttribute TaskRequest taskRequest) {
        Task task = taskService.create(taskRequest);
        return new ResponseEntity<>(taskService.transferToDto(task), HttpStatus.RESET_CONTENT);
    }

    @ApiOperation(value = "Получить задание по id")
    @GetMapping("")
    public ResponseEntity<TaskDto> getById(@RequestParam(name = "id") String id) {
        Task task = taskService.find(Long.parseLong(id));
        return new ResponseEntity<>(taskService.transferToDto(task), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Получить задание по категории и тексту задания")
    @GetMapping("/find")
    public ResponseEntity<TaskDto> findByCategoryAndTask(@RequestParam(name = "categoryid") String categoryId,
                                                         @RequestParam(name = "task") String taskContext) {
        Task task = taskService.findByCategoryIdAndTask(Long.parseLong(categoryId), taskContext);
        return new ResponseEntity<>(taskService.transferToDto(task), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Получить все задания по категории")
    @GetMapping("/all")
    public ResponseEntity<Dtos<TaskDto>> findAllByCategoryName(@RequestParam(name = "categoryName") String categoryName) {
        List<Task> tasks = taskService.findAllByCategoryName(categoryName);
        return new ResponseEntity<>(taskService.transferToDtos(tasks), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Сохранить задание")
    @PostMapping("/update")
    public ResponseEntity<TaskDto> update(@ModelAttribute TaskRequest taskRequest) {
        Task task = taskService.update(taskRequest);
        return new ResponseEntity<>(taskService.transferToDto(task), HttpStatus.RESET_CONTENT);
    }

    @ApiOperation(value = "Удалить задание")
    @GetMapping("/delete")
    public ResponseEntity<TaskDto> delete(@RequestParam(name = "id") String id) {
        Task task = taskService.delete(Long.parseLong(id));
        return new ResponseEntity<>(taskService.transferToDto(task), HttpStatus.MOVED_PERMANENTLY);
    }

    @ApiOperation(value = "Выдать случайное задание")
    @GetMapping("/random")
    public ResponseEntity<TaskDto> getRandom(@RequestParam(name = "categoryid") String categoryId) {
        Task task = taskService.getRandomTaskByCategoryId(Long.parseLong(categoryId));
        task.setAnswer("");
        return new ResponseEntity<>(taskService.transferToDto(task), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Решить задание")
    @GetMapping("/solve")
    public ResponseEntity<TaskDto> solve(@RequestParam(name = "taskid") String taskid,
                                         @RequestParam(name = "answer") String answer) {
        Task task = taskService.solve(Long.parseLong(taskid), answer);
        return new ResponseEntity<>(taskService.transferToDto(task), HttpStatus.FOUND);
    }
}
