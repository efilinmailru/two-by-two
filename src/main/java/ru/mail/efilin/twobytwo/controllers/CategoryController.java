package ru.mail.efilin.twobytwo.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.mail.efilin.twobytwo.dtos.back.responses.CategoryDto;
import ru.mail.efilin.twobytwo.dtos.back.responses.Dtos;
import ru.mail.efilin.twobytwo.dtos.front.reqiests.CategoryRequest;
import ru.mail.efilin.twobytwo.entities.Category;
import ru.mail.efilin.twobytwo.services.CategoryService;

import java.util.List;

@RestController
@RequestMapping("/categories")
@Api(tags = "Категории")
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    //    @PreAuthorize("hasPermission(@clientController.entityName, 'write' )")
    @ApiOperation(value = "Создать категорию")
    @PostMapping("/create")
    public ResponseEntity<CategoryDto> create(@ModelAttribute CategoryRequest categoryRequest) {
        Category category = categoryService.create(categoryRequest);
        return new ResponseEntity<>(categoryService.transferToDto(category), HttpStatus.RESET_CONTENT);
    }

    @ApiOperation(value = "Получить категорию по id")
    @GetMapping("")
    public ResponseEntity<CategoryDto> getById(@RequestParam(name = "id") String id) {
        Category category = categoryService.find(Long.parseLong(id));
        return new ResponseEntity<>(categoryService.transferToDto(category), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Получить категорию по имени")
    @GetMapping("/name")
    public ResponseEntity<CategoryDto> findByName(@RequestParam(name = "name") String name) {
        Category category = categoryService.findByName(name);
        return new ResponseEntity<>(categoryService.transferToDto(category), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Получить все категории")
    @GetMapping("/all")
    public ResponseEntity<Dtos<CategoryDto>> findAll() {
        List<Category> categories = categoryService.findAll();
        return new ResponseEntity<>(categoryService.transferToDtos(categories), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Сохранить категорию")
    @PostMapping("/update")
    public ResponseEntity<CategoryDto> update(@ModelAttribute CategoryRequest categoryRequest) {
        Category category = categoryService.update(categoryRequest);
        return new ResponseEntity<>(categoryService.transferToDto(category), HttpStatus.RESET_CONTENT);
    }

    @ApiOperation(value = "Удалить категорию")
    @GetMapping("/delete")
    public ResponseEntity<CategoryDto> delete(@RequestParam(name = "id") String id) {
        Category category = categoryService.delete(Long.parseLong(id));
        return new ResponseEntity<>(categoryService.transferToDto(category), HttpStatus.MOVED_PERMANENTLY);
    }
}
