package ru.mail.efilin.twobytwo.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import ru.mail.efilin.twobytwo.dtos.front.reqiests.UserRequest;
import ru.mail.efilin.twobytwo.entities.User;
import ru.mail.efilin.twobytwo.services.UserService;

@RestController
@RequestMapping("/login")
@Api(tags = "Аутентификация")
public class AuthenticationController {
    private final UserService userService;

    public AuthenticationController(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation(value = "Войти GET")
    @GetMapping("/login")
    public RedirectView loginGet(@RequestParam(name = "name") String name,
                                 @RequestParam(name = "password") String password) {
        User user = userService.findByName(name);
        if (user.getPassword().equals(password)) {
            return new RedirectView("/index.html");
        } else {
            return new RedirectView("/login");
        }
    }

    @ApiOperation(value = "Войти POST")
    @PostMapping("/login")
    public ResponseEntity<User> loginPost(@ModelAttribute UserRequest userRequest) {
        return new ResponseEntity<>(userService.find(Long.parseLong(userRequest.getId())), HttpStatus.FOUND);
    }
}
