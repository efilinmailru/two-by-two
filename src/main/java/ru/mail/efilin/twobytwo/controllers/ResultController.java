package ru.mail.efilin.twobytwo.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.mail.efilin.twobytwo.dtos.back.responses.Dtos;
import ru.mail.efilin.twobytwo.dtos.back.responses.ResultDto;
import ru.mail.efilin.twobytwo.dtos.front.reqiests.ResultRequest;
import ru.mail.efilin.twobytwo.entities.Result;
import ru.mail.efilin.twobytwo.services.ResultService;

import java.util.List;

@RestController
@RequestMapping("/result")
@Api(tags = "Результаты")
public class ResultController {
    private final ResultService resultService;

    public ResultController(ResultService resultService) {
        this.resultService = resultService;
    }

    //    @PreAuthorize("hasPermission(@clientController.entityName, 'write' )")
    @ApiOperation(value = "Создать результат")
    @PostMapping("/create")
    public ResponseEntity<ResultDto> create(@ModelAttribute ResultRequest resultRequest) {
        Result result = resultService.create(resultRequest);
        return new ResponseEntity<>(resultService.transferToDto(result), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Получить результат по id")
    @GetMapping("")
    public ResponseEntity<ResultDto> getById(@RequestParam(name = "id") long id) {
        Result result = resultService.find(id);
        return new ResponseEntity<>(resultService.transferToDto(result), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Получить результат по имени")
    @GetMapping("/name")
    public ResponseEntity<ResultDto> findByName(@RequestParam(name = "name") String name) {
        Result result = resultService.findByName(name);
        return new ResponseEntity<>(resultService.transferToDto(result), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Получить все результаты")
    @GetMapping("/all")
    public ResponseEntity<Dtos<ResultDto>> findAll() {
        List<Result> results = resultService.findAll();
        return new ResponseEntity<>(resultService.transferToDtos(results), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Сохранить результат")
    @PostMapping("/update")
    public ResponseEntity<ResultDto> update(@ModelAttribute ResultRequest resultRequest) {
        Result result = resultService.update(resultRequest);
        return new ResponseEntity<>(resultService.transferToDto(result), HttpStatus.RESET_CONTENT);
    }

    @ApiOperation(value = "Удалить результат")
    @GetMapping("/delete")
    public ResponseEntity<ResultDto> delete(@RequestParam(name = "id") long id) {
        Result result = resultService.delete(id);
        return new ResponseEntity<>(resultService.transferToDto(result), HttpStatus.MOVED_PERMANENTLY);
    }
}
