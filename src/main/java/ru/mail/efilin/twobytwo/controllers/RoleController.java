package ru.mail.efilin.twobytwo.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.mail.efilin.twobytwo.dtos.back.responses.Dtos;
import ru.mail.efilin.twobytwo.dtos.back.responses.RoleDto;
import ru.mail.efilin.twobytwo.dtos.front.reqiests.RoleRequest;
import ru.mail.efilin.twobytwo.entities.Role;
import ru.mail.efilin.twobytwo.services.RoleService;

import java.util.List;

@RestController
@RequestMapping("/roles")
@Api(tags = "Роли")
public class RoleController {
    private final RoleService roleService;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    //    @PreAuthorize("hasPermission(@clientController.entityName, 'write' )")
    @ApiOperation(value = "Создать роль")
    @PostMapping("/create")
    public ResponseEntity<RoleDto> create(@ModelAttribute RoleRequest roleRequest) {
        Role role = roleService.create(roleRequest);
        return new ResponseEntity<>(roleService.transferToDto(role), HttpStatus.RESET_CONTENT);
    }

    @ApiOperation(value = "Получить роль по id")
    @GetMapping("")
    public ResponseEntity<RoleDto> getById(@RequestParam(name = "id") String id) {
        Role role = roleService.find(Long.parseLong(id));
        return new ResponseEntity<>(roleService.transferToDto(role), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Получить роль по имени")
    @GetMapping("/name")
    public ResponseEntity<RoleDto> findByName(@RequestParam(name = "name") String name) {
        Role role = roleService.findByName(name);
        return new ResponseEntity<>(roleService.transferToDto(role), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Получить все роли")
    @GetMapping("/all")
    public ResponseEntity<Dtos<RoleDto>> findAll() {
        List<Role> roles = roleService.findAll();
        return new ResponseEntity<>(roleService.transferToDtos(roles), HttpStatus.FOUND);
    }

    @ApiOperation(value = "Сохранить роль")
    @PostMapping("/update")
    public ResponseEntity<RoleDto> update(@ModelAttribute RoleRequest roleRequest) {
        Role role = roleService.update(roleRequest);
        return new ResponseEntity<>(roleService.transferToDto(role), HttpStatus.RESET_CONTENT);
    }

    @ApiOperation(value = "Удалить роль")
    @GetMapping("/delete")
    public ResponseEntity<RoleDto> delete(@RequestParam(name = "id") String id) {
        Role role = roleService.delete(Long.parseLong(id));
        return new ResponseEntity<>(roleService.transferToDto(role), HttpStatus.MOVED_PERMANENTLY);
    }
}
