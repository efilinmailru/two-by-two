package ru.mail.efilin.twobytwo.dtos.front.reqiests;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CategoryRequest implements EntityRequest {

    @ApiModelProperty(example = "1")
    private String id;

    @ApiModelProperty(example = "Таблица умножения", required = true)
    private String name;
}
