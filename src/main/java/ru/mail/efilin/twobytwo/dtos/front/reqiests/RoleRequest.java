package ru.mail.efilin.twobytwo.dtos.front.reqiests;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class RoleRequest implements EntityRequest {

    @ApiModelProperty(example = "2")
    private String id;

    @ApiModelProperty(example = "ADMIN", required = true)
    private String name;
}
