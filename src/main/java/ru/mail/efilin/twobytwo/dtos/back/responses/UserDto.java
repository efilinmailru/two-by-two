package ru.mail.efilin.twobytwo.dtos.back.responses;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class UserDto {

    @ApiModelProperty(example = "2", required = true)
    private long id;

    @ApiModelProperty(hidden = true)
    private String name;

    @ApiModelProperty(hidden = true)
    private String password;

    @ApiModelProperty(hidden = true)
    private String firstName;

    @ApiModelProperty(hidden = true)
    private String lastName;

    @ApiModelProperty(hidden = true)
    private String email;

    @ApiModelProperty(hidden = true)
    private String phone;
}
