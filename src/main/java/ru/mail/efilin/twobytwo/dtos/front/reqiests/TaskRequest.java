package ru.mail.efilin.twobytwo.dtos.front.reqiests;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class TaskRequest implements EntityRequest {

    @ApiModelProperty(example = "2")
    private String id;

    @ApiModelProperty(example = "7x8", required = true)
    private String task;

    @ApiModelProperty(example = "56", required = true)
    private String answer;

    @ApiModelProperty(example = "1")
    private String categoryId;
}
