package ru.mail.efilin.twobytwo.dtos.front.reqiests;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class HistoryRequest implements EntityRequest {

    @ApiModelProperty(example = "2")
    private String id;

    @ApiModelProperty(example = "2", required = true)
    private String userId;

    @ApiModelProperty(example = "1", required = true)
    private String categoryId;

    @ApiModelProperty(example = "7", required = true)
    private String taskId;

    @ApiModelProperty(example = "1")
    private String resultId;

    @ApiModelProperty(example = "2021-10-11 13:24:54")
    private String created;
}
