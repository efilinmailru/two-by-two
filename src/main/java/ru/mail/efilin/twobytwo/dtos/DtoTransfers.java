package ru.mail.efilin.twobytwo.dtos;

import ru.mail.efilin.twobytwo.dtos.back.responses.Dtos;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface DtoTransfers<E, D> {

    default Dtos<D> transferToDtos (Collection<E> entities) {
        List<D> dtos = entities.stream()
                .map(this::transferToDto)
                .collect(Collectors.toList());
        return new Dtos<>(dtos);
    }

    /**
     * Entity -> DTO transfer. To implement in service.
     * @param entity to transfer.
     * @return DTO.
     */
    D transferToDto(E entity);
}
