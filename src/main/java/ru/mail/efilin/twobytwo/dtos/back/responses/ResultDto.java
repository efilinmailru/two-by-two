package ru.mail.efilin.twobytwo.dtos.back.responses;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ResultDto {

    @ApiModelProperty(example = "2", required = true)
    private long id;

    @ApiModelProperty(hidden = true)
    private String name;
}
