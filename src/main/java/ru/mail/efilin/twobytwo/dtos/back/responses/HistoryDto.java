package ru.mail.efilin.twobytwo.dtos.back.responses;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class HistoryDto {

    @ApiModelProperty(example = "2", required = true)
    private long id;

    @ApiModelProperty(hidden = true)
    private long userId;

    @ApiModelProperty(hidden = true)
    private long categoryId;

    @ApiModelProperty(hidden = true)
    private long taskId;

    @ApiModelProperty(hidden = true)
    private long resultId;

    @ApiModelProperty(hidden = true)
    private LocalDate created;
}
