package ru.mail.efilin.twobytwo.dtos.front.reqiests;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class UserRequest implements EntityRequest {

    @ApiModelProperty(example = "2")
    private String id;

    @ApiModelProperty(example = "superdog84", required = true)
    private String name;

    @ApiModelProperty(example = "123", required = true)
    private String password;

    @ApiModelProperty(example = "John")
    private String firstName;

    @ApiModelProperty(example = "Doe")
    private String lastName;

    @ApiModelProperty(example = "doggy1984@mail.ru")
    private String email;

    @ApiModelProperty(example = "+7(999) 888-77-66")
    private String phone;

    @ApiModelProperty(example = "1,2")
    private String roleIds;
}
