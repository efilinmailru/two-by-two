package ru.mail.efilin.twobytwo.dtos.back.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class Dtos<E> {

    List<E> dtos;
}
