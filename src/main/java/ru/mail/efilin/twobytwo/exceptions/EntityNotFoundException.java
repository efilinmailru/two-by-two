package ru.mail.efilin.twobytwo.exceptions;

public class EntityNotFoundException extends RuntimeException {
    static final long serialVersionUID = 1L;

    public EntityNotFoundException() {
    }

    public EntityNotFoundException(String message) {
        super(message);
    }
}
