package ru.mail.efilin.twobytwo.exceptions;

public class EntityAlreadyExistsException extends RuntimeException {
    static final long serialVersionUID = 1L;

    public EntityAlreadyExistsException() {
    }

    public EntityAlreadyExistsException(String message) {
        super(message);
    }
}
