package ru.mail.efilin.twobytwo.utils;

public interface Utils {

    default boolean isEmpty(String s) {
        return s == null || s.isEmpty();
    }

    default boolean isNotEmpty(String s) {
        return !isEmpty(s);
    }

    default boolean isEmptyId(String s) {
        return s == null || s.isEmpty() || "0".equals(s);
    }

    default boolean isNotEmptyId(String s) {
        return !isEmptyId(s);
    }
}
