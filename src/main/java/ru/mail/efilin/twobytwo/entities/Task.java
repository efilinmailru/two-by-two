package ru.mail.efilin.twobytwo.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "tasks")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "task", nullable = false)
    private String task;

    @Column(name = "answer", nullable = false)
    private String answer;

    @Column(name = "category_id", nullable = false)
    private Long categoryId;
}
