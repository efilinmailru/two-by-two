package ru.mail.efilin.twobytwo.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.mail.efilin.twobytwo.dtos.DtoTransfers;
import ru.mail.efilin.twobytwo.dtos.back.responses.UserDto;
import ru.mail.efilin.twobytwo.dtos.front.reqiests.UserRequest;
import ru.mail.efilin.twobytwo.entities.Role;
import ru.mail.efilin.twobytwo.entities.User;
import ru.mail.efilin.twobytwo.repositories.EntityRepository;
import ru.mail.efilin.twobytwo.repositories.UserRepository;
import ru.mail.efilin.twobytwo.utils.Utils;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserService extends AbstractService<User, UserRequest>
        implements Utils, DtoTransfers<User, UserDto> {
    private final RoleService roleService;

    public UserService(EntityRepository<User> repository, RoleService roleService) {
        super(repository);
        this.roleService = roleService;
    }

    public User findByName(String name) {
        UserRepository repository = (UserRepository) getRepository();
        Optional<User> optional = repository.findByName(name);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            log.error("Entity '{}' not found", name);
            return null;
        }
    }

    public User findByEmail(String email) {
        UserRepository userRepository = (UserRepository) getRepository();
        Optional<User> optional = userRepository.findByEmail(email);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            log.error("User with email '{}' not found", email);
            return null;
        }
    }

    public List<Role> findAllRolesByUserId(long userId) {
        User user = find(userId);
        if (user != null) {
            return new ArrayList<>(user.getRoles());
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    protected User requestToEntity(UserRequest request) {
        return User.builder().name(request.getName())
                .password(request.getPassword())
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .phone(request.getPhone())
                .roles(toRoles(request.getRoleIds()))
                .build();
    }

    @Override
    protected void refreshEntity(User entity, UserRequest request) {
        entity.setName(request.getName());
        entity.setPassword(request.getPassword());
        entity.setFirstName(request.getFirstName());
        entity.setLastName(request.getLastName());
        entity.setEmail(request.getEmail());
        entity.setPhone(request.getPhone());
        entity.setRoles(toRoles(request.getRoleIds()));
    }

    @Override
    public UserDto transferToDto(User entity) {
        return UserDto.builder().id(entity.getId())
                .name(entity.getName())
                .password(entity.getPassword())
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .email(entity.getEmail())
                .phone(entity.getPhone())
                .build();
    }

    private Set<Role> toRoles(String roles) {
        return Arrays.stream(roles.split(Role.DELIMITER))
                .map(id -> roleService.find(Long.parseLong(id)))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }
}
