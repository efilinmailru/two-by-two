package ru.mail.efilin.twobytwo.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.mail.efilin.twobytwo.dtos.DtoTransfers;
import ru.mail.efilin.twobytwo.dtos.back.responses.RoleDto;
import ru.mail.efilin.twobytwo.dtos.front.reqiests.RoleRequest;
import ru.mail.efilin.twobytwo.entities.Role;
import ru.mail.efilin.twobytwo.repositories.EntityRepository;
import ru.mail.efilin.twobytwo.repositories.RoleRepository;
import ru.mail.efilin.twobytwo.utils.Utils;

import java.util.Optional;

@Service
@Slf4j
public class RoleService extends AbstractService<Role, RoleRequest>
        implements Utils, DtoTransfers<Role, RoleDto> {

    public RoleService(EntityRepository<Role> repository) {
        super(repository);
    }

    public Role findByName(String name) {
        RoleRepository repository = (RoleRepository) getRepository();
        Optional<Role> optional = repository.findByName(name);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            log.error("Entity '{}' not found", name);
            return null;
        }
    }

    @Override
    protected Role requestToEntity(RoleRequest request) {
        return Role.builder().name(request.getName())
                .build();
    }

    @Override
    protected void refreshEntity(Role entity, RoleRequest request) {
        entity.setName(request.getName());
    }

    @Override
    public RoleDto transferToDto(Role entity) {
        return RoleDto.builder().id(entity.getId())
                .name(entity.getName())
                .build();
    }
}
