package ru.mail.efilin.twobytwo.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.mail.efilin.twobytwo.dtos.DtoTransfers;
import ru.mail.efilin.twobytwo.dtos.back.responses.ResultDto;
import ru.mail.efilin.twobytwo.dtos.front.reqiests.ResultRequest;
import ru.mail.efilin.twobytwo.entities.Result;
import ru.mail.efilin.twobytwo.repositories.EntityRepository;
import ru.mail.efilin.twobytwo.repositories.ResultRepository;
import ru.mail.efilin.twobytwo.utils.Utils;

import java.util.Optional;

@Service
@Slf4j
public class ResultService extends AbstractService<Result, ResultRequest>
        implements Utils, DtoTransfers<Result, ResultDto> {

    public ResultService(EntityRepository<Result> repository) {
        super(repository);
    }

    public Result findByName(String name) {
        ResultRepository repository = (ResultRepository) getRepository();
        Optional<Result> optional = repository.findByName(name);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            log.error("Entity '{}' not found", name);
            return null;
        }
    }

    @Override
    protected Result requestToEntity(ResultRequest request) {
        return Result.builder().name(request.getName())
                .build();
    }

    @Override
    protected void refreshEntity(Result entity, ResultRequest request) {
        entity.setName(request.getName());
    }

    @Override
    public ResultDto transferToDto(Result entity) {
        return ResultDto.builder().id(entity.getId())
                .name(entity.getName())
                .build();
    }
}
