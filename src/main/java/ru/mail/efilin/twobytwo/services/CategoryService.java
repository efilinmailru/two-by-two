package ru.mail.efilin.twobytwo.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.mail.efilin.twobytwo.dtos.DtoTransfers;
import ru.mail.efilin.twobytwo.dtos.back.responses.CategoryDto;
import ru.mail.efilin.twobytwo.dtos.front.reqiests.CategoryRequest;
import ru.mail.efilin.twobytwo.entities.Category;
import ru.mail.efilin.twobytwo.repositories.CategoryRepository;
import ru.mail.efilin.twobytwo.repositories.EntityRepository;
import ru.mail.efilin.twobytwo.utils.Utils;

import java.util.Optional;

@Service
@Slf4j
public class CategoryService extends AbstractService<Category, CategoryRequest>
        implements Utils, DtoTransfers<Category, CategoryDto> {

    public CategoryService(EntityRepository<Category> repository) {
        super(repository);
    }

    public Category findByName(String name) {
        CategoryRepository repository = (CategoryRepository) getRepository();
        Optional<Category> optional = repository.findByName(name);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            log.error("Entity '{}' not found", name);
            return null;
        }
    }

    @Override
    protected Category requestToEntity(CategoryRequest request) {
        return Category.builder().name(request.getName())
                .build();
    }

    @Override
    protected void refreshEntity(Category entity, CategoryRequest request) {
        entity.setName(request.getName());
    }

    @Override
    public CategoryDto transferToDto(Category entity) {
        return CategoryDto.builder().id(entity.getId())
                .name(entity.getName())
                .build();
    }

}
