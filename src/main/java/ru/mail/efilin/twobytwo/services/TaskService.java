package ru.mail.efilin.twobytwo.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.mail.efilin.twobytwo.dtos.DtoTransfers;
import ru.mail.efilin.twobytwo.dtos.back.responses.TaskDto;
import ru.mail.efilin.twobytwo.dtos.front.reqiests.HistoryRequest;
import ru.mail.efilin.twobytwo.dtos.front.reqiests.TaskRequest;
import ru.mail.efilin.twobytwo.entities.Category;
import ru.mail.efilin.twobytwo.entities.Result;
import ru.mail.efilin.twobytwo.entities.Task;
import ru.mail.efilin.twobytwo.repositories.EntityRepository;
import ru.mail.efilin.twobytwo.repositories.TaskRepository;
import ru.mail.efilin.twobytwo.utils.Utils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class TaskService extends AbstractService<Task, TaskRequest>
        implements Utils, DtoTransfers<Task, TaskDto> {
    private final CategoryService categoryService;
    private final ResultService resultService;
    private final HistoryService historyService;

    public TaskService(EntityRepository<Task> repository,
                       CategoryService categoryService,
                       ResultService resultService,
                       HistoryService historyService) {
        super(repository);
        this.categoryService = categoryService;
        this.resultService = resultService;
        this.historyService = historyService;
    }

    public List<Task> findAllByCategoryName(String categoryName) {
        Category category = categoryService.findByName(categoryName);
        if (category != null) {
            TaskRepository repository = (TaskRepository) getRepository();
            return repository.findAllByCategoryId(category.getId());
        } else {
            return new ArrayList<>();
        }
    }

    public Task findByCategoryIdAndTask(long categoryId, String task) {
        TaskRepository repository = (TaskRepository) getRepository();
        Optional<Task> optional = repository.findByCategoryIdAndTask(categoryId, task);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            log.error("Entity '{}' with category id='{}' not found", task, categoryId);
            return null;
        }
    }

    public Task getRandomTaskByCategoryId(long categoryId) {
        TaskRepository repository = (TaskRepository) getRepository();
        List<Task> tasks = repository.findAllByCategoryId(categoryId);
        int randomIndex = (int) (tasks.size() * Math.random());
        return tasks.isEmpty() ? null : tasks.get(randomIndex);
    }

    public Task solve(long taskId, String answer) {
        TaskRepository repository = (TaskRepository) getRepository();
        Task task = repository.getById(taskId);
        assert answer != null;
        Result result = answer.equals(task.getAnswer()) ? resultService.findByName("SUCCESS")
                : resultService.findByName("FAILURE");
        if (result == null) {
            log.error("Entity 'Result' not found");
            return null;
        }
        HistoryRequest historyRequest = createHistoryRequest(task, result);
        if (historyRequest == null) {
            log.error("Error creating entity 'HistoryRequest'");
            return null;
        }
        historyService.create(historyRequest);
        return task;
    }

    private HistoryRequest createHistoryRequest(Task task, Result result) {
        return HistoryRequest.builder()
                .categoryId(String.valueOf(task.getCategoryId()))
                .taskId(String.valueOf(task.getId()))
                .resultId(String.valueOf(result.getId()))
                .userId("1") //TODO: implement user id
                .created(String.valueOf(LocalDate.now()))
                .build();
    }

    @Override
    protected Task requestToEntity(TaskRequest request) {
        return Task.builder().task(request.getTask())
                .answer(request.getAnswer())
                .categoryId(Long.parseLong(request.getCategoryId()))
                .build();
    }

    @Override
    protected void refreshEntity(Task entity, TaskRequest request) {
        entity.setTask(request.getTask());
        entity.setAnswer(request.getAnswer());
        entity.setCategoryId(Long.parseLong(request.getCategoryId()));
    }

    @Override
    public TaskDto transferToDto(Task entity) {
        return TaskDto.builder().id(entity.getId())
                .task(entity.getTask())
                .answer(entity.getAnswer())
                .categoryId(entity.getCategoryId())
                .build();
    }
}
