package ru.mail.efilin.twobytwo.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.efilin.twobytwo.dtos.front.reqiests.EntityRequest;
import ru.mail.efilin.twobytwo.exceptions.EntityNotFoundException;
import ru.mail.efilin.twobytwo.repositories.EntityRepository;
import ru.mail.efilin.twobytwo.utils.Utils;

import java.util.List;
import java.util.Optional;

@Slf4j
public abstract class AbstractService<E, R extends EntityRequest> implements Utils {
    private final EntityRepository<E> repository;

    public AbstractService(EntityRepository<E> repository) {
        this.repository = repository;
    }

    @Transactional
    public E create(R request) {
        request.setId("");
        return repository.save(requestToEntity(request));
    }

    public List<E> findAll() {
        return repository.findAll();
    }

    public E find(long id) {
        Optional<E> optional = repository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            logError("Entity with id='{}' not found", id);
            return null;
        }
    }

    @Transactional
    public E update(R request) {
        if (isEmptyId(request.getId())) {
            return create(request);
        }
        Optional<E> optional = repository.findById(Long.parseLong(request.getId()));
        if (optional.isPresent()) {
            E entity = optional.get();
            refreshEntity(entity, request);
            return repository.save(entity);
        } else {
            logError("No entity with id='{}'", request.getId());
            throw new EntityNotFoundException();
        }
    }

    @Transactional
    public E delete(long id) {
        E entity = find(id);
        repository.deleteById(id);
        return entity;
    }

    protected abstract E requestToEntity(R request);

    protected abstract void refreshEntity(E entity, R request);

    private void logError(String msg, long id) {
        log.error(msg, id);
    }

    private void logError(String msg, String id) {
        log.error(msg, id);
    }

    public EntityRepository<E> getRepository() {
        return repository;
    }

/*private String getParameterClass() {
        ArrayList<E> listOfNumbers = new FloatList();
        Class actualClass = listOfNumbers.getClass();
        ParameterizedType type = (ParameterizedType)actualClass.getGenericSuperclass();
        Class parameter = (Class)type.getActualTypeArguments()[0];
        return parameter.getSimpleName();
    }

    private class FloatList extends ArrayList<E>{}*/
}
