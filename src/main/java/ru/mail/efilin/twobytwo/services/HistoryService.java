package ru.mail.efilin.twobytwo.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.mail.efilin.twobytwo.dtos.DtoTransfers;
import ru.mail.efilin.twobytwo.dtos.back.responses.HistoryDto;
import ru.mail.efilin.twobytwo.dtos.front.reqiests.HistoryRequest;
import ru.mail.efilin.twobytwo.entities.History;
import ru.mail.efilin.twobytwo.repositories.EntityRepository;
import ru.mail.efilin.twobytwo.repositories.HistoryRepository;
import ru.mail.efilin.twobytwo.utils.Utils;

import java.time.LocalDate;
import java.util.List;

@Service
@Slf4j
public class HistoryService extends AbstractService<History, HistoryRequest>
        implements Utils, DtoTransfers<History, HistoryDto> {

    public HistoryService(EntityRepository<History> repository) {
        super(repository);
    }

    public List<History> findAll(long userId, long categoryId, long taskId, long resultId, LocalDate created) {
        HistoryRepository repository = (HistoryRepository) getRepository();
        return repository.findAllByUserIdAndCategoryIdAndTaskIdAndResultIdAndCreated(
                userId, categoryId, taskId, resultId, created);
    }

    @Override
    protected History requestToEntity(HistoryRequest request) {
        return History.builder().userId(Long.parseLong(request.getUserId()))
                .categoryId(Long.parseLong(request.getCategoryId()))
                .taskId(Long.parseLong(request.getTaskId()))
                .resultId(Long.parseLong(request.getResultId()))
                .created(LocalDate.parse(request.getCreated()))
                .build();
    }

    @Override
    protected void refreshEntity(History entity, HistoryRequest request) {
        entity.setUserId(Long.parseLong(request.getUserId()));
        entity.setCategoryId(Long.parseLong(request.getCategoryId()));
        entity.setTaskId(Long.parseLong(request.getTaskId()));
        entity.setResultId(Long.parseLong(request.getResultId()));
        entity.setCreated(LocalDate.parse(request.getCreated()));
    }

    @Override
    public HistoryDto transferToDto(History entity) {
        return HistoryDto.builder().id(entity.getId())
                .userId(entity.getUserId())
                .categoryId(entity.getCategoryId())
                .taskId(entity.getTaskId())
                .resultId(entity.getResultId())
                .created(entity.getCreated())
                .build();
    }

}
