package ru.mail.efilin.twobytwo.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.mail.efilin.twobytwo.dtos.back.responses.Dtos;
import ru.mail.efilin.twobytwo.dtos.back.responses.UserDto;
import ru.mail.efilin.twobytwo.entities.User;
import ru.mail.efilin.twobytwo.services.UserService;

import java.util.List;

/**
 * https://www.baeldung.com/spring-boot-security-autoconfiguration
 * https://www.baeldung.com/java-config-spring-security
 * https://www.baeldung.com/spring-security-login
 * https://docs.spring.io/spring-boot/docs/2.4.3/reference/htmlsingle/#boot-features-security
 * https://spring.io/guides/gs/securing-web/
 * https://www.tutorialspoint.com/spring_boot/spring_boot_securing_web_applications.html
 */
@Configuration
@EnableWebSecurity
public class BasicConfiguration extends WebSecurityConfigurerAdapter {
    private final UserService userService;
//    private final DataSource dataSource;

    public BasicConfiguration(UserService userService) {
        this.userService = userService;
//        this.dataSource = dataSource;
    }

//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        log.info("===F= configureGlobal auth: " + auth);
//        PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
//        auth.jdbcAuthentication().dataSource(dataSource)
//                .usersByUsernameQuery(
//                        "select name, password, email enabled from users where email=?")
//                .authoritiesByUsernameQuery(
//                        "select name, role from user_roles where user_id=?")
//                .withUser("efilin@mail.ru").password(passwordEncoder.encode("grumpy67")).roles("USER")
//                .and()
//                .withUser("admin@server.com").password(passwordEncoder.encode("admin")).roles("USER", "ADMIN");
//    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        List<User> users = userService.findAll();
        Dtos<UserDto> userDtos = userService.transferToDtos(users);
        for (UserDto userDto : userDtos.getDtos()) {
            auth
                    .inMemoryAuthentication()
                    .withUser(userDto.getEmail())
                    .password(encoder.encode(userDto.getPassword())).roles("USER")
                    .and();
        }
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/login").permitAll()
                .anyRequest().authenticated()
                .and().formLogin()
                .loginPage("/login").permitAll()
                .and().logout().permitAll();
    }
}
