-- CONFIG USERS
INSERT INTO users (name, password, email)
VALUES ('admin', 'admin', 'admin@server.com');

INSERT INTO users (name, first_name, last_name, password, email)
VALUES ('kotofey', N'Евгений', N'Филин', 'grumpy', 'efilin@mail.ru');

-- CONFIG ROLES
INSERT INTO roles (id, name)
VALUES (1, 'ADMIN');

INSERT INTO roles (id, name)
VALUES (2, 'USER');

INSERT INTO user_roles (user_id, role_id)
VALUES (1, 1);

INSERT INTO user_roles (user_id, role_id)
VALUES (1, 2);

INSERT INTO user_roles (user_id, role_id)
VALUES (2, 1);

-- CONFIG RESULTS
INSERT INTO results (id, name)
VALUES (1, 'SUCCESS');

INSERT INTO results (id, name)
VALUES (2, 'FAILURE');

-- CONFIG CATEGORIES
INSERT INTO categories (id, name)
VALUES (1, N'Таблица умножения');
