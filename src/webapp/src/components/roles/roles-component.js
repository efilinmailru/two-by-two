import {MenuItem, Select} from "@material-ui/core";
import {useState} from "react";

export const RolesComponent = ({allUsers,
                                   currentUserId, setCurrentUserId,
                                   currentUserRoles, setCurrentUserRoles}) => {
    const [init, setInit] = useState(true);

    const refreshUserRoles = (userId) => {
        fetch("/users/roles?userid=" + userId)
            .then(resp => resp.json())
            .then(userRoles => setCurrentUserRoles(userRoles.dtos))
            .catch(err => console.log("---err", err));
    }

    const refreshUserIdAndRoles = (event) => {
        setCurrentUserId(event.target.value);
        refreshUserRoles(event.target.value);
    }

    if (init) {
        setInit(false);
        refreshUserRoles(currentUserId);
    }

    return (
        <>
            <div>
                <div>
                    Роли пользователя
                </div>
                <div>
                    <Select key="usersList" value={currentUserId} onChange={refreshUserIdAndRoles}>
                        {
                            allUsers.map(user =>
                                <MenuItem key={"user-" + user.id} value={user.id}>
                                    {user.name}
                                </MenuItem>
                            )
                        }
                    </Select>
                </div>
                <table>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Роли</th>
                    </tr>
                    </thead>
                    <tbody>
                    {currentUserRoles.map((row) => (
                        <tr key={row.id}>
                            <td align="right">{row.id}</td>
                            <td>{row.name}</td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </div>
        </>
    )
}
