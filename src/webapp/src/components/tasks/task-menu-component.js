export const TaskMenuComponent = ({categoryId, setCategoryId, setItemValue, setMessage}) => {
    let idElement = document.getElementById('task-id');
    let taskId = idElement == null ? '' : idElement.value;
    let taskElement = document.getElementById('task-task');
    let taskTask = taskElement == null ? '' : taskElement.value;
    let answerElement = document.getElementById('task-answer');
    let taskAnswer = answerElement == null ? '' : answerElement.value;

    const deleteTask = () => {
        fetch("/tasks/delete?id=" + taskId)
            .then(resp => resp.json())
            .then(deletedTaskJson => {
                if (deletedTaskJson.task === undefined) {
                    setMessage('Задание с ID ="' + taskId + '" не найдено');
                } else {
                    deletedTaskJson.id = '';
                    fillTaskForm(deletedTaskJson);
                    setMessage(deletedTaskJson.task + ' удалено');
                }
            })
            .catch(err => {
                console.log("---err", err);
                setMessage(err);
            });
    }

    const findTask = () => {
        fetch("/tasks?id=" + taskId)
            .then(resp => resp.json())
            .then(foundTaskJson => {
                if (foundTaskJson.task === undefined) {
                    setMessage('Задание с ID ="' + taskId + '" не найдено');
                } else {
                    fillTaskForm(foundTaskJson);
                    setMessage('Задание "' + foundTaskJson.task + '" из категории "' + foundTaskJson.categoryId + '" найдено');
                    // setCategoryId(foundTaskJson.categoryId.toString());
                }
            })
            .catch(err => {
                console.log("---err", err);
                setMessage(err);
            });
    }

    const findTaskByTask = () => {
        fetch("/tasks/find?categoryid=" + categoryId + "&task=" + taskTask)
            .then(resp => resp.json())
            .then(foundTaskJson => {
                if (foundTaskJson.task === undefined) {
                    setMessage('Задание "' + taskTask + '" из категории "' + categoryId + '" не найдено');
                } else {
                    fillTaskForm(foundTaskJson);
                    setMessage('Задание "' + foundTaskJson.task + '" из категории "' + categoryId + '" найдено');
                }
            })
            .catch(err => {
                console.log("---err", err);
                setMessage(err);
            });
    }

    const getRandomTaskByCategoryId = () => {
        fetch("/tasks/random?categoryid=" + categoryId)
            .then(resp => resp.json())
            .then(foundTaskJson => {
                if (foundTaskJson.task === undefined) {
                    setMessage('Системная ошибка при поиске случайного задания из категории "' + categoryId + '"');
                } else {
                    fillTaskForm(foundTaskJson);
                    setMessage('Случайное задание "' + foundTaskJson.task + '" из категории "' + categoryId + '" найдено');
                }
            })
            .catch(err => {
                console.log("---err", err);
                setMessage(err);
            });
    }

    const solveTask = () => {
        fetch("/tasks/solve?taskid=" + taskId + "&answer=" + taskAnswer)
            .then(resp => resp.json())
            .then(foundTaskJson => {
                if (foundTaskJson.task === undefined) {
                    setMessage('Системная ошибка при отправке задания "' + taskAnswer + '" на проверку');
                } else {
                    setMessage('Решение "' + foundTaskJson.answer + '" отправлено на проверку');
                }
            })
            .catch(err => {
                console.log("---err", err);
                setMessage(err);
            });
        getRandomTaskByCategoryId();
    }

    const fillTaskForm = (json) => {
        try {
            if (json !== null) {
                setItemValue('task-id', json.id);
                setItemValue('task-task', json.task);
                setItemValue('task-answer', json.answer);
                setCategoryId(json.categoryId.toString());
            }
        } catch (e) {
            alert(e.message);
            console.log(e);
        }
    }

    return (
        <>
            Действия
            <table width="100%">
                <tbody>
                <tr hidden={taskAnswer !== '' || (taskId !== '' && taskTask !== '')}>
                    <td>
                        <button onClick={getRandomTaskByCategoryId}>Следующее задание</button>
                    </td>
                </tr>
                <tr hidden={taskId === '' || taskAnswer === ''}>
                    <td colSpan='2'>
                        <button type="submit" onClick={solveTask}>Отправить решение</button>
                    </td>
                </tr>
                <tr hidden={taskId === ''}>
                    <td>
                        <button onClick={deleteTask}>Удалить {taskId}</button>
                    </td>
                </tr>
                <tr hidden={taskId === ''}>
                    <td>
                        <button onClick={findTask}>Найти {taskId}</button>
                    </td>
                </tr>
                <tr hidden={taskTask === '' || categoryId === ''}>
                    <td>
                        <button onClick={findTaskByTask}>Найти "{taskTask}"</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </>
    )
}
