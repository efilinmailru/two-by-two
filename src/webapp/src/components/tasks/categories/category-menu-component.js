export const CategoryMenuComponent = ({refreshAllCategories, setItemValue, setMessage}) => {
    let idElement = document.getElementById('category-id');
    let categoryId = idElement == null ? '' : idElement.value;
    let nameElement = document.getElementById('category-name');
    let categoryName = nameElement == null ? '' : nameElement.value;

    const deleteCategory = () => {
        fetch("/categories/delete?id=" + categoryId)
            .then(resp => resp.json())
            .then(deletedCategoryJson => {
                if (deletedCategoryJson.name === undefined) {
                    setMessage('Категория с ID ="' + categoryId + '" не найдена');
                } else {
                    deletedCategoryJson.id = '';
                    fillCategoryForm(deletedCategoryJson);
                    setMessage(deletedCategoryJson.name + ' удалена');
                }
            })
            .catch(err => {
                console.log("---err", err);
                setMessage(err);
            });
        refreshAllCategories();
    }

    const findCategory = () => {
        fetch("/categories?id=" + categoryId)
            .then(resp => resp.json())
            .then(foundCategoryJson => {
                if (foundCategoryJson.name === undefined) {
                    setMessage('Категория с ID =' + categoryId + ' не найдена');
                } else {
                    fillCategoryForm(foundCategoryJson);
                    setMessage('Категория "' + foundCategoryJson.name + '" найдена');
                }
            })
            .catch(err => {
                console.log("---err", err);
                setMessage(err);
            });
    }

    const findCategoryByName = () => {
        fetch("/categories/find?name=" + categoryName)
            .then(resp => resp.json())
            .then(foundCategoryJson => {
                if (foundCategoryJson.name === undefined) {
                    setMessage('Категория "' + categoryName + '" не найдена');
                } else {
                    fillCategoryForm(foundCategoryJson);
                    setMessage('Категория "' + foundCategoryJson.name + '" найдена');
                }
            })
            .catch(err => {
                console.log("---err", err);
                setMessage(err);
            });
    }

    const fillCategoryForm = (json) => {
        try {
            if (json !== null) {
                setItemValue('category-id', json.id);
                setItemValue('category-name', json.name);
            }
        } catch (e) {
            alert(e.message);
            console.log(e);
        }
    }

    return (
        <>
            Действия
            <table width='100%'>
                <tbody>
                <tr hidden={categoryId===''}>
                    <td>
                        <button onClick={deleteCategory}>Удалить {categoryId}</button>
                    </td>
                </tr>
                <tr hidden={categoryId===''}>
                    <td>
                        <button onClick={findCategory}>Найти {categoryId}</button>
                    </td>
                </tr>
                <tr hidden={categoryName===''}>
                    <td>
                        <button onClick={findCategoryByName}>Найти "{categoryName}"</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </>
    )
}

