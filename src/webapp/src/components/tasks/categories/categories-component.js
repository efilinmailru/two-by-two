import {CategoryItemsComponent} from "./category-items-component";
import {CategoryMenuComponent} from "./category-menu-component";

export const CategoriesComponent = ({refreshAllCategories, setItemValue, setMessage}) => {

    return (
        <>
            <table width='40%'>
                <tbody>
                <tr>
                    <td width='50%'>
                        <CategoryItemsComponent refreshAllCategories={refreshAllCategories}
                                                setMessage={setMessage} />
                    </td>
                    <td width='50%'>
                        <CategoryMenuComponent refreshAllCategories={refreshAllCategories}
                                               setItemValue={setItemValue} setMessage={setMessage} />
                    </td>
                </tr>
                </tbody>
            </table>
        </>
    )
}
