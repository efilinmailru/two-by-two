export const CategoryItemsComponent = ({refreshAllCategories, setMessage}) => {
    let idElement = document.getElementById('category-id');
    let categoryId = idElement == null ? '' : idElement.value;

    const saveCategory = () => {
        refreshAllCategories();
        setMessage("Категория '" + document.getElementById('category-name').value + "' сохранена");
    }

    return (
        <>
            Категория
            <form action={window.location.origin + "/categories/update"} method="post">
                <table width='100%'>
                    <tbody>
                    <tr>
                        <td><label htmlFor="id">ID</label></td>
                        <td><input type="text" name="id" id="category-id"/></td>
                    </tr>
                    <tr>
                        <td><label htmlFor="category">Название</label></td>
                        <td><input type="text" name="name" id='category-name'/></td>
                    </tr>
                    <tr hidden={categoryId===''}>
                        <td colSpan='2'>
                            <button type="submit" onClick={saveCategory}>Сохранить</button>
                        </td>
                    </tr>
                    <tr hidden={categoryId!==''}>
                        <td colSpan='2'>
                            <button type="submit" onClick={saveCategory}>Создать</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </>
    )
}

