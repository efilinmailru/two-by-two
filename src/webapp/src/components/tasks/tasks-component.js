import {TaskItemsComponent} from "./task-items-component";
import {useState} from "react";
import {TaskMenuComponent} from "./task-menu-component";
import {CategoriesComponent} from "./categories/categories-component";

export const TasksComponent = ({allCategories, setAllCategories,
                                   categoryId, setCategoryId,
                                   setItemValue, setMessage}) => {
    const [init, setInit] = useState(true);

    const refreshAllCategories = () => {
        fetch("/categories/all")
            .then(resp => resp.json())
            .then(allCategoriesData => setAllCategories(allCategoriesData.dtos))
            .catch(err => console.log("---err", err));
    }

    if (init) {
        setInit(false);
        refreshAllCategories();
    }

    return (
        <>
            <table width='40%'>
                <tbody>
                <tr>
                    <td width='50%'>
                        <TaskItemsComponent allCategories={allCategories}
                                            categoryId={categoryId} setCategoryId={setCategoryId}
                                            refreshAllCategories={refreshAllCategories} setMessage={setMessage} />
                    </td>
                    <td width='50%'>
                        <TaskMenuComponent categoryId={categoryId} setCategoryId={setCategoryId}
                                           setItemValue={setItemValue} setMessage={setMessage} />
                    </td>
                </tr>
                </tbody>
            </table>
            <br/>
            <CategoriesComponent refreshAllCategories={refreshAllCategories}
                                 setItemValue={setItemValue} setMessage={setMessage} />
        </>
    )
}
