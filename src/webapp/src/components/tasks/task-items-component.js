import {MenuItem, Select} from "@material-ui/core";

export const TaskItemsComponent = ({allCategories,
                                       categoryId, setCategoryId,
                                       refreshAllCategories, setMessage}) => {
    let idElement = document.getElementById('task-id');
    let taskId = idElement == null ? '' : idElement.value;
    let taskElement = document.getElementById('task-task');
    let taskTask = taskElement == null ? '' : taskElement.value;
    let answerElement = document.getElementById('task-answer');
    let taskAnswer = answerElement == null ? '' : answerElement.value;

    const saveTask = () => {
        setMessage("Задание '" + taskTask + "' сохранено в категории "
            + categoryId);
    }

    const getCategories = (event) => {
        refreshAllCategories();
        setCategoryId(event.target.value);
    }

    return (
        <>
            Карточка задания
            <form action={window.location.origin + "/tasks/update"} method="post">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td><label htmlFor="id">ID</label></td>
                        <td><input type="text" name="id" id="task-id"/></td>
                    </tr>
                    <tr>
                        <td><label htmlFor="task">Задание</label></td>
                        <td><input type="text" name="task" id='task-task'/></td>
                    </tr>
                    <tr>
                        <td><label htmlFor="answer">Ответ</label></td>
                        <td><input type="text" name="answer" id='task-answer'/></td>
                    </tr>
                    <tr>
                        <td><label htmlFor="categoryId">Категория</label></td>
                        <td>
                            <Select key="categories" value={categoryId} onChange={getCategories}
                                    name='categoryId' id='task-category-id'>
                                {
                                    allCategories.map(category =>
                                        <MenuItem key={"task-category-id-" + category.id} value={category.id}>
                                            {category.name}
                                        </MenuItem>
                                    )
                                }
                            </Select>
                        </td>
                    </tr>
                    <tr hidden={taskId === '' || taskTask === '' || taskAnswer === ''}>
                        <td colSpan='2'>
                            <button type="submit" onClick={saveTask}>Сохранить</button>
                        </td>
                    </tr>
                    <tr hidden={taskId !== '' || taskTask === '' || taskAnswer === ''}>
                        <td colSpan='2'>
                            <button type="submit" onClick={saveTask}>Создать</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </>
    )
}
