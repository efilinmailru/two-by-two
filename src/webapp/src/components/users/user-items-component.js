export const UserItemsComponent = ({setMessage}) => {
    const getUpdateAction = window.location.origin + "/users/update";
    let idElement = document.getElementById('user-id');
    let userId = idElement == null ? '' : idElement.value;

    const saveUser = () => {
        setMessage("Пользователь " + userId + " сохранен");
    }

    return (
        <>
            Карточка пользователя
            <form action={getUpdateAction} method="post">
                <table width='100%'>
                    <tbody>
                    <tr>
                        <td><label htmlFor="id">ID</label></td>
                        <td><input type="text" name="id" id="user-id"/></td>
                    </tr>
                    <tr>
                        <td><label htmlFor="name">Псевдоним</label></td>
                        <td><input type="text" name="name" id='user-name'/></td>
                    </tr>
                    <tr>
                        <td><label htmlFor="password">Пароль</label></td>
                        <td><input type="password" name="password" id='user-password'/></td>
                    </tr>
                    <tr>
                        <td><label htmlFor="firstName">Имя</label></td>
                        <td><input type="text" name="firstName" id='user-firstname'/></td>
                    </tr>
                    <tr>
                        <td><label htmlFor="lastName">Фамилия</label></td>
                        <td><input type="text" name="lastName" id='user-lastname'/></td>
                    </tr>
                    <tr>
                        <td><label htmlFor="email">E-mail</label></td>
                        <td><input type="email" name="email" id='user-email'/></td>
                    </tr>
                    <tr>
                        <td><label htmlFor="phone">Телефон</label></td>
                        <td><input type="text" name="phone" id='user-phone'/></td>
                    </tr>
                    <tr>
                        <td>
                            Роли
                        </td>
                        <td>
                            <div>
                                ADMIN<input type="checkbox" name="roleIds" value="1" />
                            </div>
                            <div>
                                USER<input type="checkbox" name="roleIds" value="2" checked />
                            </div>
                        </td>
                    </tr>
                    <tr hidden={userId===''}>
                        <td colSpan='2'>
                            <button type="submit" onClick={saveUser}>Сохранить</button>
                        </td>
                    </tr>
                    <tr hidden={userId!==''}>
                        <td colSpan='2'>
                            <button type="submit" onClick={saveUser}>Создать</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </>
    )
}

