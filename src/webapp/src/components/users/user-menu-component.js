export const UserMenuComponent = ({setItemValue, setMessage}) => {
    let idElement = document.getElementById('user-id');
    let userId = idElement == null ? '' : idElement.value;
    let emailElement = document.getElementById('user-email');
    let email = emailElement == null ? '' : emailElement.value;

    const deleteUser = () => {
        fetch("/users/delete?id=" + userId)
            .then(resp => resp.json())
            .then(deletedUserJson => {
                if (deletedUserJson.name === undefined) {
                    setMessage('Пользователь ' + userId + ' не найден');
                } else {
                    deletedUserJson.id = '';
                    fillUserForm(deletedUserJson);
                    setMessage('Пользователь "' + deletedUserJson.name + '" удален');
                }
            })
            .catch(err => {
                console.log("---err", err);
                setMessage(err);
            });
    }

    const findUser = () => {
        fetch("/users?id=" + userId)
            .then(resp => resp.json())
            .then(foundUserJson => {
                if (foundUserJson.name === undefined) {
                    setMessage('Пользователь ' + userId + ' не найден');
                } else {
                    fillUserForm(foundUserJson);
                    setMessage('Пользователь "' + foundUserJson.name + ' найден');
                }
            })
            .catch(err => {
                console.log("---err", err);
                setMessage(err);
            });
    }

    const findUserByEmail = () => {
        fetch("/users/find?email=" + email)
            .then(resp => resp.json())
            .then(foundUserJson => {
                if (foundUserJson.name === undefined) {
                    setMessage('Пользователь "' + email + '" не найден');
                } else {
                    fillUserForm(foundUserJson);
                    setMessage('Пользователь "' + email + '" найден');
                }
            })
            .catch(err => {
                console.log("---err", err);
                setMessage(err);
            });
    }

    const fillUserForm = (json) => {
        try {
            if (json !== null) {
                setItemValue('user-id', json.id);
                setItemValue('user-name', json.name);
                setItemValue('user-password', json.password);
                setItemValue('user-firstname', json.firstName);
                setItemValue('user-lastname', json.lastName);
                setItemValue('user-email', json.email);
                setItemValue('user-phone', json.phone);
                setItemValue('user-roles', json.roles);
            }
        } catch (e) {
            alert(e.message);
            console.log(e);
        }
    }

    return (
        <>
            Действия
            <table width='100%'>
                <tbody>
                <tr hidden={userId===''}>
                    <td>
                        <button onClick={deleteUser}>Удалить {userId}</button>
                    </td>
                </tr>
                <tr hidden={userId===''}>
                    <td>
                        <button onClick={findUser}>Найти {userId}</button>
                    </td>
                </tr>
                <tr hidden={email===''}>
                    <td>
                        <button onClick={findUserByEmail}>Найти "{email}"</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </>
    )
}

