import {RolesComponent} from "../roles/roles-component";
import {UserItemsComponent} from "./user-items-component";
import {UserMenuComponent} from "./user-menu-component";
import {useState} from "react";

export const UsersComponent = ({allUsers, setAllUsers,
                                   currentUserId, setCurrentUserId,
                                   currentUserRoles, setCurrentUserRoles,
                                   setItemValue, setMessage}) => {
    const [init, setInit] = useState(true);

    const refreshAllUsers = () => {
        fetch("/users/all")
            .then(resp => resp.json())
            .then(allUsersData => setAllUsers(allUsersData.dtos))
            .catch(err => console.log("---err", err));
    }

    if (init) {
        setInit(false);
        refreshAllUsers();
    }

    return (
        <>
            <table width='40%'>
                <tbody>
                <tr>
                    <td width='33%'>
                        <UserItemsComponent setMessage={setMessage} />
                    </td>
                    <td>
                        <UserMenuComponent setMessage={setMessage} setItemValue={setItemValue} />
                    </td>
                    <td width='33%'>
                        <RolesComponent allUsers={allUsers}
                                        currentUserId={currentUserId} setCurrentUserId={setCurrentUserId}
                                        currentUserRoles={currentUserRoles} setCurrentUserRoles={setCurrentUserRoles}
                                        refreshAllUsers={refreshAllUsers} />
                    </td>
                </tr>
                </tbody>
            </table>
        </>
    )
}

