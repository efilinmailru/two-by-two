import './App.css';
import {useEffect, useState} from "react";
import {UsersComponent} from "./components/users/users-component";
import {HomeComponent} from "./components/home/home-component";
import {TasksComponent} from "./components/tasks/tasks-component";

const App = () => {
    const [init, setInit] = useState(true);
    const [date, setDate] = useState(null);
    const [time, setTime] = useState(null);
    const [allUsers, setAllUsers] = useState(null);
    const [currentUserId, setCurrentUserId] = useState(null);
    const [currentUserRoles, setCurrentUserRoles] = useState(null);
    const [page, setPage] = useState(null);
    const [message, setMessage] = useState(null);
    const [allCategories, setAllCategories] = useState(null);
    const [categoryId, setCategoryId] = useState(null);

    if (init) {
        setInit(false);
        setDate(new Date().toLocaleDateString());
        setTime(new Date().toLocaleTimeString());
        setAllUsers([{"id":"1","name":"admin"}]);
        setCurrentUserId("1");
        setCurrentUserRoles([{"id":"1","name":"ADMIN"},{"id":"2","name":"USER"}]);
        setPage("home");
        setAllCategories([{"id":"1","name":"Таблица умножения"}]);
        setCategoryId("1");
    }

    useEffect(
        () => {
            const timerId = setInterval(() => {
                setDate(new Date().toLocaleDateString());
                setTime(new Date().toLocaleTimeString());
            }, 1000);
            return () => clearInterval(timerId);
        }, []);

    const handleShowHome = () => {
        setPage("home");
        document.getElementById('home-button').setAttribute('class', 'link-button active-menu');
        document.getElementById('user-button').setAttribute('class', 'link-button');
        document.getElementById('task-button').setAttribute('class', 'link-button');
    }

    const handleShowUser = () => {
        setPage("user");
        document.getElementById('home-button').setAttribute('class', 'link-button');
        document.getElementById('user-button').setAttribute('class', 'link-button active-menu');
        document.getElementById('task-button').setAttribute('class', 'link-button');
    }

    const handleShowTask = () => {
        setPage("task");
        document.getElementById('home-button').setAttribute('class', 'link-button');
        document.getElementById('user-button').setAttribute('class', 'link-button');
        document.getElementById('task-button').setAttribute('class', 'link-button active-menu');
    }

    const setItemValue = (itemName, value) => {
        let item = document.getElementById(itemName);
        if (item !== null) {
            item.value = value === undefined ? '' : value;
        }
    }

    return (
        <div className="App">
            <div>
                <div className="align-left">{date} {time}</div>
                <table width="50%" >
                    <thead>
                    <tr>
                        <th width="33%">
                            <button id='home-button' class='link-button active-menu' onClick={handleShowHome}>Дом</button>
                        </th>
                        <th>
                            <button id='task-button' class='link-button' onClick={handleShowTask}>Задания</button>
                        </th>
                        <th width="33%">
                            <button id='user-button' class='link-button' onClick={handleShowUser}>Пользователи</button>
                        </th>
                    </tr>
                    </thead>
                </table>
            </div>
            <br/>
            <div hidden={page !== 'home'}>
                <HomeComponent />
            </div>
            <div hidden={page !== 'task'}>
                <TasksComponent allCategories={allCategories} setAllCategories={setAllCategories}
                                categoryId={categoryId} setCategoryId={setCategoryId}
                                setItemValue={setItemValue} setMessage={setMessage} />
            </div>
            <div hidden={page !== 'user'}>
                <UsersComponent allUsers={allUsers} setAllUsers={setAllUsers}
                                currentUserId={currentUserId} setCurrentUserId={setCurrentUserId}
                                currentUserRoles={currentUserRoles} setCurrentUserRoles={setCurrentUserRoles}
                                setItemValue={setItemValue} setMessage={setMessage}
                />
            </div>
            <h2 id='message'>{message}</h2>
        </div>
    );
}

export default App;
